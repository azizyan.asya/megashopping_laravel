<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    protected $table = 'post_like';
    
    public static  function _save($request){
        $like = new PostLike();
        $like->customer_id = $request->customer_id;
        $like->post_id = $request->post_id;
        $like->status = 'like';
        $like->save();
        return $like;
    }

}
