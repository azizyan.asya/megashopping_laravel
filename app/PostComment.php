<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostComment extends Model
{
    protected $table = 'post_comment';

    public static  function _save($request){
        $comment = new PostComment();
        $comment->customer_id = $request->customer_id;
        $comment->post_id = $request->post_id;
        $comment->comment = $request->comment;
        $comment->image = $request->image;
        $comment->save();
        return $comment;
    }

    public function getImageAttribute($value)
    {
        return url('/').'/images/comment/'.$value;
    }

}
