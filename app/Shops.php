<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shops extends Model
{
    protected $table = 'shops';

    public function location()
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        return $this->hasMany('App\ShopLocation','shop_id')->limit($limit)->offset($offset);
    }

    public function getLogoAttribute($value)
    {
        return url('/').'/images/'.$value;
    }
    
    public function getBgImageAttribute($value)
    {
        return url('/').'/images/'.$value;
    }

}
