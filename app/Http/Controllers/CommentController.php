<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PostProduct;
use App\PostComment;
use PhpParser\Comment;
use Illuminate\Support\Facades\File;
use Validator;
class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *A
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'post_id' => 'required',
            'comment' => 'required',
        ]);
        if ($request->file('image')){
            $image = $request->file('image');
            $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('/images/comment');
            $image->move($destinationPath, $input['imagename']);
            $request->image = $input['imagename'];
        }
        if ($validator->fails()) {
            $data = ['Error'=>$validator->errors()];
            return $data;
        }
        $comment = PostComment::_save($request);
        return $comment;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $post_comment = PostComment::where('post_id',$id)->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
        if(!$post_comment->isEmpty()){
            return $post_comment;
        }
        return ['Error'=>'Invalid post id'];

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $comment = PostComment::find($request->id);
        if($comment) {
            $url = explode('public/',$comment->image);
            File::delete( $url[1]);
            $comment->delete();
            return ['Message'=>'Success'];
        }
        return ['Error'=>'Invalid comment id'];
    }


    public  function comment(Request $request){

        $limit = $request->limit;
        $offset =$request->offset;
        $post_comment = PostComment::where('post_id',$request->id)->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
        if(!$post_comment->isEmpty()){
            return $post_comment;
        }
        return ['Error'=>'Invalid post id'];
    }
}
