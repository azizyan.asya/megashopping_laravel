<?php

namespace App\Http\Controllers;

use App\CustomerEntity;
use App\UserFollow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FollowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer_id = CustomerEntity::find($request->customer_id);
        if($customer_id){
            $follow_id = CustomerEntity::find($request->follow_id);
            if($follow_id){
                UserFollow::_save($request);
                return ['Message'=>'Success'];
            }
        }
        return ['Error'=>'Invalid customer_id or follow_id'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];

        $follows_id = UserFollow::where('customer_id', $id)->offset($offset)->limit($limit)->get();
        if(!$follows_id->isEmpty()){
            foreach ($follows_id as $follow){
                $follow_id = $follow->id;
                $customer = CustomerEntity::find($follow->follow_id);
                $follow_count =  UserFollow::where('customer_id', $follow-> follow_id)->count('id');
                $users  = DB::select("SELECT group_concat(VALUE SEPARATOR ' ') AS fullname FROM `customer_entity_varchar`  WHERE entity_id =$customer->entity_id AND attribute_id IN (5,7) ");

                foreach($users as $user){
                    $fullname =  $user->fullname;
                }

                $data[] = ['follow_id'=>$follow_id,'follow_count'=> $follow_count ,'customer'=> ['id'=>$customer->entity_id,'fullname'=> $fullname,'email'=>$customer->email]];
            }
            return $data;
        }
        return ["Error'=>'You don't have follows"];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $follow_id[] =$id;
        $follows_id = UserFollow::where('customer_id', $id)->get();
        foreach ($follows_id as $follow){
            $follow_id[] = $follow-> follow_id;
        }
        $customers  = CustomerEntity::whereNotIn('entity_id', $follow_id)->offset($offset)->limit($limit)->get();
        foreach ($customers as $customer){
            $follow_count =  UserFollow::where('customer_id', $customer-> entity_id)->count('id');
            $users  = DB::select("SELECT group_concat(VALUE SEPARATOR ' ') AS fullname FROM `customer_entity_varchar`  WHERE entity_id =$customer->entity_id AND attribute_id IN (5,7) ");

            foreach($users as $user){
                $fullname =  $user->fullname;
            }
            $data[] = ['follow_count'=> $follow_count ,'customer'=> ['id'=>$customer->entity_id,'fullname'=> $fullname,'email'=>$customer->email]];
        }
        return $data;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $follow = UserFollow::find($request->id);
        if($follow){
            $follow->delete();
            return ['Message'=>'Success'];
        }
        return ['Error'=>'Invalid follow id'];
    }


    public function user(Request $request){
        $limit = $request->limit;
        $offset = $request->offset;
        $follow_id[] =$request->id;
        $follows_id = UserFollow::where('customer_id', $request->id)->get();
        foreach ($follows_id as $follow){
            $follow_id[] = $follow-> follow_id;
        }
        $customers  = CustomerEntity::whereNotIn('entity_id', $follow_id)->offset($offset)->limit($limit)->get();
        foreach ($customers as $customer){
            $follow_count =  UserFollow::where('customer_id', $customer-> entity_id)->count('id');
            $users  = DB::select("SELECT group_concat(VALUE SEPARATOR ' ') AS fullname FROM `customer_entity_varchar`  WHERE entity_id =$customer->entity_id AND attribute_id IN (5,7) ");

            foreach($users as $user){
                $fullname =  $user->fullname;
            }
            $data[] = ['follow_count'=> $follow_count ,'customer'=> ['id'=>$customer->entity_id,'fullname'=> $fullname,'email'=>$customer->email]];
        }
        return $data;
    }
    
    public function user_list(Request $request){
        $limit = $request->limit;
        $offset = $request->offset;
        $follow_id[] =$request->id;
        $follows_id = UserFollow::where('customer_id', $request->id)->get();
        foreach ($follows_id as $follow){
            $follow_id[] = $follow-> follow_id;
        }
        $customers  = CustomerEntity::whereNotIn('entity_id', $follow_id)->offset($offset)->limit($limit)->get();
        foreach ($customers as $customer){
            $follow_count =  UserFollow::where('customer_id', $customer-> entity_id)->count('id');
            $users  = DB::select("SELECT group_concat(VALUE SEPARATOR ' ') AS fullname FROM `customer_entity_varchar`  WHERE entity_id =$customer->entity_id AND attribute_id IN (5,7) ");

            foreach($users as $user){
                $fullname =  $user->fullname;
            }
            $data[] = ['follow_count'=> $follow_count ,'customer'=> ['id'=>$customer->entity_id,'fullname'=> $fullname,'email'=>$customer->email]];
        }
        return $data;

    }
}
