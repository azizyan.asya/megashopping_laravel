<?php

namespace App\Http\Controllers;

use App\PostLike;
use Illuminate\Http\Request;
use Validator;
use  App\PostProduct;
class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'customer_id' => 'required',
            'post_id' => 'required',
        ]);

        if ($validator->fails()) {
            $data = ['Error'=>$validator->errors()];
            return $data;
        }
        $like = PostLike::_save($request);
        return ['Message'=>'Success'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $user_likes = PostLike::where('customer_id',$id)->where('status','like')->get();

        if(!$user_likes->isEmpty()){
            foreach ($user_likes as $user_like){
                $post_id[] = $user_like->post_id;
            }
            $post = PostProduct::whereIn('id' ,$post_id)->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
            return $post;
        }
        return ['Error'=>'Invalid customer id'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return 1;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $like = PostLike::find($request->id);
        if($like){

           $like->status = 'dislike';
            $like->save();
            return ['Message'=>'Success'];
        }
        return ['Error'=>'Invalid like id'];
    }
    
    
    public  function like(Request $request){
        $limit = $request->limit;
        $offset =$request->offset;
        $user_likes = PostLike::where('customer_id',$request->id)->where('status','like')->get();
        if(!$user_likes->isEmpty()){
            foreach ($user_likes as $user_like){
                $post_id[] = $user_like->post_id;
            }

            $posts = PostProduct::whereIn('id' ,$post_id)->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
            foreach ($posts as $post){
                $data['like'] = PostLike::where('post_id',$post->id)->where('status','like')->count('id');
                $data['post'] = $post;
                $dates[] = $data;
            }

            return $dates;

        }
        return ['Error'=>'Invalid customer id'];
    }
}
