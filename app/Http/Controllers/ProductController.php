<?php

namespace App\Http\Controllers;

use App\CustomerEntity;
use App\PostComment;
use App\PostLike;
use App\PostProduct;
use Illuminate\Http\Request;
use PhpParser\Comment;
use Validator;
use Illuminate\Support\Facades\File;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $posts = PostProduct::where('status','on')->offset($offset)->limit($limit)->get();
        foreach ($posts as $post){
            $data['like'] = PostLike::where('post_id',$post->id)->where('status','like')->count('id');
            $data['post'] = $post;
            $dates[] = $data;
        }

        return $dates;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'post_image' => 'required|image',
            'product' => 'required',
            'customer_id' => 'required',
            'post_name' => 'required',
            'title' => 'required',
        ]);

        if ($validator->fails()) {
            $data = ['Error'=>$validator->errors()];
            return $data;
        }
        $image = $request->file('post_image');
        $input['imagename'] = time().'.'.$image->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $image->move($destinationPath, $input['imagename']);
        $request->post_image = $input['imagename'];
        $product = PostProduct::_save($request);
        return $product;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\ResponsehasMany
     */
    public function show($id)
    {

        $customer = CustomerEntity::where('entity_id',$id)->first();
        if($customer){
            foreach ($customer->post_product as $post){
                $like = PostLike::where('post_id',$post->id)->count('id');
                $data[] = ['like'=> $like,'post'=> $post];
            }
            return $data;
        }
        return ['Error'=>'Invalid customer id'];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        $user_likes = PostLike::where('customer_id',$id)->get();
        if(!$user_likes->isEmpty()){
            foreach ($user_likes as $user_like){
                $post_id[] = $user_like->post_id;
            }
            $post = PostProduct::whereNotIn('id' ,$post_id)->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
            return $post;
        }
        return ['Error'=>'Invalid customer id'];
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id )
    {
        return 6;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

        $product = PostProduct::find($request->id);
        if($product){
            $comments =  PostComment::where('post_id',$id)->get();
            foreach ($comments as $comment){
                $comment_url = explode('public/',$comment->image);
                File::delete( $comment_url[1]);
                $comment->delete();
            }
            PostLike::where('post_id',$id)->delete();
            $url = explode('public/',$product->post_image);
            File::delete( $url[1]);
            $product->delete();
            return 'success';
        }
        return ['Error'=>'Invalid post id'];
    }

    public  function likes(Request $request){
        $limit = $request->limit;
        $offset =$request->offset;
        $user_likes = PostLike::where('customer_id',$request->id)->get();
        if(!$user_likes->isEmpty()){
            foreach ($user_likes as $user_like){
                $post_id[] = $user_like->post_id;
            }
            $posts = PostProduct::whereNotIn('id' ,$post_id)->where('status','on')->orderBy('created_at', 'desc')->offset($offset)->limit($limit)->get();
            foreach ($posts as $post){
                $data['like'] = PostLike::where('post_id',$post->id)->where('status','like')->count('id');
                $data['post'] = $post;
                $dates[] = $data;
            }

            return $dates;
        }
        return ['Error'=>'Invalid customer id'];
    }

    public  function user(Request $request){

        $customer = CustomerEntity::where('entity_id',$request->id)->first();
        if($customer){
            foreach ($customer->post_product as $post){
                $data['like'] = PostLike::where('post_id',$post->id)->where('status','like')->count('id');
                $data['post'] = $post;
                $datas[] = $data;
            }
            return $datas;
        }
        return ['Error'=>'Invalid customer id'];

    }

}
