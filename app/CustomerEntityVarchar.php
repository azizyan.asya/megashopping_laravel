<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerEntityVarchar extends Model
{
    protected $table = 'customer_entity_varchar';
}
