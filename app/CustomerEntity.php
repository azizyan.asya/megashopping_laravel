<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomerEntity extends Model
{
    protected $table = 'customer_entity';
    protected $primaryKey = 'entity_id';

    public function post_product()
    {
        $limit = $_GET['limit'];
        $offset = $_GET['offset'];
        return $this->hasMany('App\PostProduct','customer_id')->limit($limit)->offset($offset);
    }

}
