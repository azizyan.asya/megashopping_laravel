<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserFollow extends Model
{
    protected $table = 'user_follow';

    public static  function _save($request){
        $follow = new UserFollow();
        $follow->customer_id = $request->customer_id;
        $follow->follow_id = $request->follow_id;
        $follow->save();
        return $follow;
    }
}
