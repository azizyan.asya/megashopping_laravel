<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostProduct extends Model
{
    protected $table = 'post_product';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'title', 'post_image','post_name','product','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'status','created_at','updated_at'
    ];
    

    public function customer()
    {
        return $this->belongsTo('App\CustomerEntity','customer_id');
    }

    public static function _save($request){
        $post = new PostProduct();
        $post->customer_id = $request->customer_id;
        $post->title = $request->title;
        $post->post_image = $request->post_image;
        $post->post_name = $request->post_name;
        $post->product = $request->product;
        $post->status = 'off';
        $post->save();
        return $post;
    }

    public function getPostImageAttribute($value)
    {
       return url('/').'/images/'.$value;
    }
    public function getProductAttribute($value){
        return json_decode($value);
    }

}
