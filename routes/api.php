<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('post', 'ProductController');
Route::post('post/user', 'ProductController@user');
Route::post('post/like/id', 'ProductController@likes');
Route::resource('comment', 'CommentController');
Route::post('comment/id', 'CommentController@comment');
Route::resource('like', 'LikeController');
Route::post('like/id', 'LikeController@like');
Route::resource('shop', 'ShopsController');
Route::post('shop/id', 'ShopsController@shop');
Route::resource('follow', 'FollowController');
Route::post('follow/id', 'FollowController@user');
Route::post('follow/user/list', 'FollowController@user_list');
